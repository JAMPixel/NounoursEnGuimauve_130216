//
// Created by stiven on 14/12/15.
//

#include "Texture.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
GLuint Texture::getName() const {
    return text;
}


void Texture::bindTexture() {
    glBindTexture(GL_TEXTURE_2D,text);

}

Texture::Texture(std::string image) {
    SDL_Surface * surface = IMG_Load(image.c_str());
    glGenTextures(1,&text);
    glBindTexture(GL_TEXTURE_2D,text);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,surface->w,surface->h,0,GL_RGBA,GL_UNSIGNED_BYTE,surface->pixels);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    SDL_FreeSurface(surface);
    glBindTexture(GL_TEXTURE_2D,0);
}

Texture::Texture() {

}

Texture::~Texture() {
    glDeleteTextures(1,&text);
}
