//
// Created by stiven aigle on 18/01/16.
//

#include <iostream>
#include <array>
#include "quad.hpp"
#include "erreur.hpp"



const std::vector<glm::vec3> mat::rect
            {{0.0f, 0.0f, 1.0f},
                     {0.0f, 1.0f, 1.0f},
                     {1.0f, 0.0f, 1.0f},
                     {1.0f, 1.0f, 1.0f}
             };
const std::vector<glm::vec2> mat::uv
            {{0.0f, 0.0f},
                     {0.0f, 1.0f},
                     {1.0f, 0.0f},
                     {1.0f, 1.0f}
             };

glm::mat3 normalizedScreenSpace(const float height, const float width) {
    glm::mat3 mat(
            2.0f/width,0.0f,0.0f,
            0.0f,-2.0f/height,0.0f,
            -1.0f,1.0f,1.0f
    );
    return mat;
}


glm::mat3 transform(transformStorage const &pat,float offsetX,float offsetY) { //use evocative name to help you navigate in your code
    float cos0 = std::cos(pat.angle*(float)M_PI/180);
    float sin0 = std::sin(pat.angle*(float)M_PI/180);
    glm::mat3 mat (1.0f,0.0f,0.0f,
                    0.0f,1.0f,0.f,
                    -0.5f,-0.5f,1.0f);
    glm::mat3 truc{pat.scale.x*cos0 ,-pat.scale.x*sin0,0.0f,
            pat.scale.y*sin0,pat.scale.y*cos0,0.0f,
            -offsetX*(pat.scale.x*cos0+pat.scale.y*sin0)+pat.tran.x ,-offsetY*(-pat.scale.x*sin0+pat.scale.y*cos0)+pat.tran.y, 1.0f
    };
    return truc;
}

void display(const glm::mat3 &mat) {
    for(int i =0;i<3;i++){
        for(int j=0;j<3;j++){
            std::cout << mat[j][i]<<" ";
        }
        std::cout <<"\n";
    }

}