#version 330 core

out vec4 color;
in vec3 color_out;
in vec2 uv_out;
in float rayon_out;
uniform sampler2D nosTextures;
uniform int choix;
in vec2 uvPerso_out;

void main(){
    if(choix==1){
        float alpha = 1.;
        float rayon = sqrt(uv_out[0]*uv_out[0]+uv_out[1]*uv_out[1]);
        if (rayon > rayon_out){
            alpha=0.;
        }else{
            alpha*=(rayon-0.35);
        }
        color = vec4(color_out,alpha);
    }else if(choix == 0){
        color = texture(nosTextures,uvPerso_out,1.).rgba;
       //color = vec4(1.,0.,0.,1.);
    }else{
         float alpha = 1.;
                float rayon = sqrt(uv_out[0]*uv_out[0]+uv_out[1]*uv_out[1]);
                if (rayon > rayon_out){
                    alpha=0.;
                }else{
                    alpha*=(rayon-0.35);
                }
                color = vec4(1.,0.,0.,alpha);
    }

}

