//
// Created by stiven aigle on 27/01/16.
//
#include "Vbo.hpp"
#include "erreur.hpp"

Vbo::Vbo() {

}


Vbo::Vbo(GLuint &vao, GLint index, GLint size, const std::vector<glm::vec3> &data) {
    glBindVertexArray(vao); getError();
    glGenBuffers(1,&vbo); getError();
    glBindBuffer(GL_ARRAY_BUFFER,vbo); getError();
    glBufferData(GL_ARRAY_BUFFER,data.size()*sizeof(glm::vec3),data.data(),GL_STATIC_DRAW);
    getError();
    glEnableVertexAttribArray(index); getError();
    glVertexAttribPointer(
            index,
            size,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
    );
}

Vbo::Vbo(GLuint &vao, GLint index, GLint size, const std::vector<glm::vec2> &data) {
    glBindVertexArray(vao); getError();
    glGenBuffers(1,&vbo); getError();
    glBindBuffer(GL_ARRAY_BUFFER,vbo); getError();
    glBufferData(GL_ARRAY_BUFFER,data.size()*sizeof(glm::vec2),data.data(),GL_STATIC_DRAW);
    getError();
    glEnableVertexAttribArray(index); getError();
    glVertexAttribPointer(
            index,
            size,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
    );

 getError();
}

Vbo::~Vbo() {
    glDeleteBuffers(1,&vbo);
}

void Vbo::deleteBuffer() {
    glDeleteBuffers(1,&vbo);
}


GLuint Vbo::getVbo() {
    return vbo;
}
