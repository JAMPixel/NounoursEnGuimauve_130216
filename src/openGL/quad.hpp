//
// Created by stiven aigle on 18/01/16.
//

#ifndef OPENGL_MATRICE_HPP
#define OPENGL_MATRICE_HPP

#include <glm/matrix.hpp>
#include <array>
#include <GL/glew.h>
#include <vector>
namespace mat {
    extern const std::vector<glm::vec3> rect;
    extern const std::vector<glm::vec2> uv;

}

struct transformStorage {
    glm::vec2 tran=glm::vec2(0.0f);
    glm::vec2 scale=glm::vec2(1.0f);
    double angle = 0.0f;
};
glm::mat3 transform(transformStorage const &pat,float offsetX,float offsetY);
void display(glm::mat3 const &mat);
glm::mat3 normalizedScreenSpace(const float height,const float width);







#endif //OPENGL_MATRICE_HPP
