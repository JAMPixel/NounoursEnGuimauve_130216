//
// Created by stiven chouette on 27/01/16.
//

#ifndef OPENGL_VBO_HPP
#define OPENGL_VBO_HPP


#include <glm/matrix.hpp>
#include <array>
#include <vector>
#include <GL/glew.h>

class Vbo {
private:
    GLuint vbo;

public:
    Vbo();
    Vbo(GLuint &vao, GLint index, GLint size, const std::vector<glm::vec3> &data);
    Vbo(GLuint &vao, GLint index, GLint size, const std::vector<glm::vec2> &data);
    Vbo(Vbo const& )=delete;
    ~Vbo();
    void deleteBuffer();
    GLuint getVbo();


};


#endif //OPENGL_VBO_HPP
