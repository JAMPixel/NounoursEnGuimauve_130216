#version 330 core

layout (location = 0) in vec3 vertices;
layout (location = 1) in vec2 uv;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 uvPerso;

out vec2 uv_out;
out vec2 uvPerso_out;

uniform mat3 scale;
uniform mat3 transformation;
uniform float rayon;
uniform vec2 hg;
uniform vec2 bd;
out float rayon_out;
out vec3 color_out;
void main(){
    gl_Position = vec4(scale * transformation * vertices, 1.);
    uv_out = uv;
    rayon_out = 1.;
    color_out = color;
    uvPerso_out = mix(hg,hg+bd,uvPerso);

}