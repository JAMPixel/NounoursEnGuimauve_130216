//
// Created by stiven aigle on 16/12/15.
//

#ifndef OPENGL_INITOPENGL_HPP
#define OPENGL_INITOPENGL_HPP

#include <SDL2/SDL.h>

class InitOGL{

    private:
        SDL_Window *window ;
        SDL_GLContext context;
    public:
        InitOGL(const int larg, const int haut, const char* titre);
        InitOGL (InitOGL const &) = delete;
        SDL_Window *getWindow()const ;
        ~InitOGL();

};


#endif //OPENGL_INITOPENGL_HPP
