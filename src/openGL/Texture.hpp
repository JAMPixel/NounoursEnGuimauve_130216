//
// Created by stiven on 14/12/15.
//

#ifndef OPENGL_TEXTURE_HPP
#define OPENGL_TEXTURE_HPP
#include <GL/glew.h>
#include <string>

//use PNG
class Texture{
    private:
        GLuint text;

    public:
        GLuint getName() const;
        Texture(std::string image);
        Texture();
    //    Texture(Texture const&)= delete;
        void bindTexture();
        ~Texture();



};
#endif //OPENGL_TEXTURE_HPP
