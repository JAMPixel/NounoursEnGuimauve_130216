//
// Created by stiven aigle on 16/12/15.
//

#include "InitOGL.hpp"
#include "erreur.hpp"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <iostream>
#include <stdexcept>



InitOGL::InitOGL(const int larg, const int haut, const char *titre) {
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS)){
        std::string erreur = SDL_GetError();
        throw std::runtime_error("erreur initialisation SDL "+ erreur);
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3); // version 3
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3); // version .3 -> version 3.3
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );//active le double buffering
    window = SDL_CreateWindow(titre,SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED ,larg,haut,SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL );
    if(!window){
        std::string erreur = SDL_GetError();
        SDL_Quit();
        throw std::runtime_error("probleme init window "+erreur);
    }
    context=SDL_GL_CreateContext(window); // création context openGL
    // initialisation de glew
    glewExperimental = true; // super, giga, mega important //in french, you put a space only before compound punctuation mark (;:!?)
    if(glewInit()!= GLEW_OK){
        SDL_Quit();
        throw std::runtime_error("probleme initialisation glew");
    }
    glGetError();
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    getError();
    glEnable(GL_BLEND);
    getError();
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    getError();

}

InitOGL::~InitOGL() {
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();

}



SDL_Window *InitOGL::getWindow() const {
    return window;
}
