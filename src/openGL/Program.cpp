//
// Created by stiven on 14/12/15.
//

#include "Program.hpp"
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>
#include "erreur.hpp"

Program::Program() {

}

void Program::useProgram() {
    glUseProgram(programID);
    getError();

}

void getError(){
    GLenum error = glGetError();
    if(error){
        std::cout << gluErrorString(error)<<std::endl;
        assert(false);
    }
}

GLuint Program::getProgram() {
    return programID;
}

Program::~Program() {
    glDeleteProgram(programID);
}

Program::Program(char const* vertexShader, char const * fragmentShader) {

    // Création des Shaders
    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    getError();
    GLuint framentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    getError();
    // Lit le code source du vertex Shader
    std::string vertexShaderCode;
    std::ifstream vertexShaderStream(vertexShader, std::ios::in);
    std::string vertex(vertexShader);
    if(!vertexShaderStream.is_open()) {
        throw std::runtime_error("probleme ouverture " + vertex );
    }
    std::string lu ;
    while (getline(vertexShaderStream, lu)){
        vertexShaderCode += "\n" + lu;//permet au compilo de donner la bonne ligne d'erreur
    }
    // pas besoin de close a la main , il le fera tout seul comme un grand a la fin du constructeur (ça s'appelle un scope)
    //scope == toute paire d'accolade

    // LECTURE DU FICHIER FRAGMENT
    std::string fragmentShaderCode;
    std::ifstream fragmentShaderStream(fragmentShader,std::ios::in);
    std::string fragment(fragmentShader);
    if(!fragmentShaderStream.is_open()){
       throw std::runtime_error("probleme ouverture "+fragment);
    }
    while(getline(fragmentShaderStream,lu)){
        fragmentShaderCode+="\n" +lu; // on stocke le code dans fragmentShaderCode
    }
    GLint Result = GL_FALSE;
    int InfoLogLength;
    // on compile Vertex Shader    getError();

    printf("Compiling shader : %s\n", vertexShader);
    char const * VertexSourcePointer = vertexShaderCode.c_str();
    glShaderSource(vertexShaderID, 1, &VertexSourcePointer , NULL);
    getError();

    glCompileShader(vertexShaderID);
    getError();

    // Check Vertex Shader
    glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &Result);
    getError();
    glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    getError();
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(vertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    getError();
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }



    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragmentShader);
    char const * FragmentSourcePointer = fragmentShaderCode.c_str();
    glShaderSource(framentShaderID, 1, &FragmentSourcePointer , NULL);
    getError();
    glCompileShader(framentShaderID);
    getError();

    // Check Fragment Shader
    glGetShaderiv(framentShaderID, GL_COMPILE_STATUS, &Result);
    getError();
    glGetShaderiv(framentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(framentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
    getError();
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }



    // Link the program
    printf("Linking program\n");
    programID = glCreateProgram();
    getError();
    getError();
    glAttachShader(programID, vertexShaderID);
    getError();
    glAttachShader(programID, framentShaderID);
    getError();
    glLinkProgram(programID);
    getError();

    // Check the program
    glGetProgramiv(programID, GL_LINK_STATUS, &Result);
    getError();
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    getError();
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(programID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    getError();
    }


    glDetachShader(programID, vertexShaderID);
    getError();
    glDetachShader(programID, framentShaderID);
    getError();

    glDeleteShader(vertexShaderID);
    getError();
    glDeleteShader(framentShaderID);
    getError();


}
