//
// Created by maxime on 13/02/16.
//

#include "JouJou.hpp"
#include <random>
#include <cmath>

JouJou::JouJou()=default;

JouJou::JouJou(int nbBulle,const int xWin, const int yWin, float hPerso, float lPerso): xWindow_(xWin), yWindow_(yWin) {
    float   nextX = random()%xWin,
            nextR=50.0f;
    int i=0;
    int j=0;
    bool b = true;
    while(i<nbBulle) {
        //nextR = 30.0f+random()%80;
        while(j<(int)vBubulle_.size() && b){
            j++;
            if(vBubulle_[j].getX_()-nextR>=nextX && vBubulle_[j].getX_()+nextR <= nextX){
                b=false;
            }
        }
        if(b) {
            vBubulle_.push_back(Bubulle(nextX, yWin, nextR, 1.0f));
            i++;
        }
    }
    // TRLOLOLOLOLOLOLOL ILÉ 00:14 ET CA MARCHE
    bubulleMan_=StikMann(vBubulle_[0].getX_(),vBubulle_[0].getY_()-vBubulle_[0].getR_(), hPerso, lPerso);
}


void JouJou::popBubble() {
    float       newX=random()%(xWindow_-200)+100.0f;
                //newR = 30.0f+random()/80.0f;// reduit la fenetre d'apparition des bulles

    vBubulle_.push_back(Bubulle(newX, yWindow_, 50.0f, (float) (random() % 3) + 1.0f));

}


void JouJou::iteration() {
    if(bubulleMan_.getEnVie()) {// Si le perso est en vie
        Bubulle *bToile = nullptr;
        bToile = getBubulleDessous();
        // Cas de contacte d'une bubulle

        if (bToile != nullptr &&
            (bubulleMan_.getY() + bubulleMan_.getHaut() / 2) >= (bToile->getY_() - bToile->getR_() / 2) &&
            (bubulleMan_.getY() + bubulleMan_.getHaut() / 2) <= (bToile->getY_() - bToile->getR_() / 2 + bToile->getV_())) {

        /*if(bToile!= nullptr &&
                (bubulleMan_.getY() + bubulleMan_.getHaut()/2)>=
                        sqrt((bToile->getV_()*bToile->getV_())-(bubulleMan_.getX() - bToile->getX_())*(bubulleMan_.getX() - bToile->getX_())) &&
                (bubulleMan_.getY() + bubulleMan_.getHaut()/2)>=
                sqrt((bToile->getV_()*bToile->getV_())-(bubulleMan_.getX() - bToile->getX_())*(bubulleMan_.getX() - bToile->getX_())) + bToile->getV_()){
*/
            bubulleMan_.iteration(false, 0, bToile);

        } else if (bToile != nullptr)// Cas de test de fonctionnement
        {
            std::cout << (bubulleMan_.getY() + bubulleMan_.getHaut() / 2) << std::endl;
            std::cout << (bToile->getY_() - bToile->getR_() - bToile->getV_() - 1) << std::endl;
            std::cout << (bToile->getY_() - bToile->getR_() + bToile->getV_() + 1) << std::endl;
            std::cout << "ca chie dans la colle" << std::endl;
        }
        // Itere les bubulles
        for (std::size_t i = 0; i < vBubulle_.size(); i++) {
            if (!vBubulle_[i].iteration(xWindow_, yWindow_)) {
                vBubulle_.erase(vBubulle_.begin() + i);
            }
        }

        // Bloque le pop des bulles à 10/map
        if (Bubulle::getID() < 10) {
            int nbPop = (int) (random() % 2);
            // int nbPop = 1;
            for (int i = 0; i < nbPop; i++) {
                popBubble();
            }
        }
    }
    if(bubulleMan_.getY()>yWindow_ || !bubulleMan_.getEnVie()){
        bubulleMan_.mourirCommeUneCrepe();
    }

}

// Recupere la bulle directement en dessous du perso
Bubulle * JouJou::getBubulleDessous() {
    double distance, dMax=std::numeric_limits<double>::max();
    Bubulle * bToile= nullptr;

    for(std::size_t i=0;i<vBubulle_.size();i++){
        // Test de détection de la bulle | CA MARCHE NE PAS DERANGER.
        if( (vBubulle_[i].getY_() -  bubulleMan_.getY()) > 0.0f &&
                bubulleMan_.getX() - (vBubulle_[i].getX_()+vBubulle_[i].getR_()-1.0f) < 0.0f &&
                bubulleMan_.getX() - (vBubulle_[i].getX_()-vBubulle_[i].getR_()+1.0f)  >0.0f) {

            // Calcule de distance avec PYTHAGORE TA MERE
            distance = sqrt((bubulleMan_.getX() - vBubulle_[i].getX_() - vBubulle_[i].getR_()) *
                            (bubulleMan_.getX() - vBubulle_[i].getR_() - vBubulle_[i].getX_()) +
                            (bubulleMan_.getY() - vBubulle_[i].getY_() - vBubulle_[i].getR_()) *
                            (bubulleMan_.getY() - vBubulle_[i].getY_() - vBubulle_[i].getR_()));

            // Si la distance est meilleure que la précédente ET plus basse que 100 unités
            if (dMax > distance &&  distance<=100.0f ) {
                bToile = &vBubulle_[i];
                dMax = distance;
                std::cout<<"il y a un truc"<<std::endl;
            }

        }
    }
    if(bToile!= nullptr) // Test de présence
    {
        std::cout<<"Ya une bubulle la dessous"<<std::endl;
    }
    StikMann::setUnderBubulle_(bToile);
    return bToile;
}


// Recupere la bulle sous le perso
Bubulle * JouJou::getBubulleProche() {
    double distance=0.0f, dMax=0.0f;
    Bubulle * bToile= nullptr;
    for(std::size_t i=0;i<vBubulle_.size();i++){

        if (dMax < distance ) {
            bToile = &vBubulle_[i];
            dMax = distance;
        }

    }
    return bToile;
}


// Recupere une bulle au hasard pour la depop par les archers
int JouJou::depopBubble() {

    return (int)(random()%vBubulle_.size());

}

void JouJou::detruire(int r) {
    vBubulle_.erase(vBubulle_.begin()+r);

}



/*std::vector<Bubulle*> JouJou::getPlusProche() {
    std::vector<Bubulle*> plusProche;
    std::vector<Bubulle*> retour;
    double distance;
    double temp;
    for(int i = 0;i<vBubulle_.size();i++){
        distance = sqrt((vBubulle_[i].getX_()-bubulleMan_.getX())*(vBubulle_[i].getY_()-bubulleMan_.getX())
                        +(vBubulle_[i].getY_()-bubulleMan_.getY())*(vBubulle_[i].getY_()-bubulleMan_.getY()));
        plusProche.push_back(&vBubulle_[i]);
        for(int j =0;j<plusProche.size();j++){
            temp = sqrt((vBubulle_[j].getX_()-bubulleMan_.getX())*(vBubulle_[j].getY_()-bubulleMan_.getX())
                            +(vBubulle_[j].getY_()-bubulleMan_.getY())*(vBubulle_[j].getY_()-bubulleMan_.getY()));
            if(distance <temp){
                for(size_t k =plusProche.size()-1;k>j;k--){
                    plusProche[k]=plusProche[k-1];
                }
                break;
            }
        }
    }
    for (int i = 0;i<8;i++){
        retour.push_back(plusProche[i]);
    }
    return retour;

}*/
