//
// Created by maxime on 13/02/16.
//

#ifndef NOUNOURSENGUIMAUVE_STIKMANN_HPP
#define NOUNOURSENGUIMAUVE_STIKMANN_HPP

#include "Bubulle.hpp"

class StikMann {
private:
        float x_;
        float y_;
        float largeur_;
        float hauteur_;
        float v_;
        int compteurSaut_;
        bool saut_;
        static  Bubulle * underBubulle_;
        bool enVie;

public:

    bool isSaut() const {
        return saut_;
    }

    void setSaut(bool saut) {
        StikMann::saut_ = saut;
    }

    int getCompteurSaut_() const {
        return compteurSaut_;
    }

    void setCompteurSaut_(int compteurSaut_) {
        StikMann::compteurSaut_ = compteurSaut_;
    }

    static void setUnderBubulle_(Bubulle *underBubulle_) {
        StikMann::underBubulle_ = underBubulle_;
    }

    static Bubulle *getUnderBubulle_() {
        return underBubulle_;
    }

    StikMann();
    StikMann(float x,float y,float largeur,float hauteur);
    float getX()const;
    float getY()const;
    float getLarg() const;
    float getHaut() const;
    void setX(float x_);
    void setY(float y_);
    void tomberEtCaFaitPouf(float xBubulle, float yBubulle, float rayonBubulle);
    void sauterStyleYiha();
    void mourirCommeUneCrepe();
    bool getEnVie();

    void iteration(bool, int, Bubulle *);

};


#endif //NOUNOURSENGUIMAUVE_STIKMANN_HPP
