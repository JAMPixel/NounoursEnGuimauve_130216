//
// Created by maxime on 13/02/16.
//

#ifndef NOUNOURSENGUIMAUVE_BUBULLE_HPP
#define NOUNOURSENGUIMAUVE_BUBULLE_HPP


class Bubulle {
private :
    static int ID;
    float       x_;
    float       y_;
    float       r_;
    float       v_;

public:
    Bubulle();
    Bubulle(float x_, float y_, float r_, float v_) : x_(x_), y_(y_), r_(r_), v_(v_) { ID++; }

    Bubulle(const Bubulle&);

    static int getID() {
        return ID;
    }

    static void setID(int ID) {
        Bubulle::ID = ID;
    }

    float getV_() const {
        return v_;
    }

    void setV_(float v_) {
        Bubulle::v_ = v_;
    }

    float getX_() const {
        return x_;
    }

    void setX_(float x_) {
        Bubulle::x_ = x_;
    }

    float getY_() const {
        return y_;
    }

    void setY_(float y_) {
        Bubulle::y_ = y_;
    }

    float getR_() const {
        return r_;
    }

    void setR_(float r_) {
        Bubulle::r_ = r_;
    }

    bool iteration(int,int);

    void reception();

    ~Bubulle() { ID--;}
};


#endif //NOUNOURSENGUIMAUVE_BUBULLE_HPP
