//
// Created by maxime on 13/02/16.
//

#ifndef NOUNOURSENGUIMAUVE_JOUJOU_HPP
#define NOUNOURSENGUIMAUVE_JOUJOU_HPP

#include <iostream>
#include <vector>

#include "Bubulle.hpp"
#include "StikMann.hpp"


class JouJou {
private :
    std::vector<Bubulle> vBubulle_;
    StikMann bubulleMan_;

    int xWindow_;
    int yWindow_;

public:
    JouJou();

    JouJou(int, const int, const int, float, float);

    const std::vector<Bubulle> &getVBubulle_() const {
        return vBubulle_;
    }
    StikMann &getBubulleMan_() {
        return bubulleMan_;
    }

   /* std::vector <Bubulle*> getPlusProche();*/
    void popBubble();

    void iteration();

    Bubulle *getBubulleDessous();

    Bubulle * getBubulleProche();

    int depopBubble();
    void detruire(int r);
};


#endif //NOUNOURSENGUIMAUVE_JOUJOU_HPP
