//
// Created by maxime on 13/02/16.
//

#include <iostream>
#include "StikMann.hpp"


Bubulle * StikMann::underBubulle_ = nullptr;


StikMann::StikMann(): x_(0), y_(0){
    v_=2.0f;
}

StikMann::StikMann(float x,float y,float largeur,float hauteur): x_(x), y_(y), largeur_(largeur), hauteur_(hauteur),enVie(
        true){
    v_=2.0f;
    compteurSaut_=0;
    saut_=false;
}

float StikMann::getX()const {
    return x_;
}
float StikMann::getY() const {
    return y_;
}

void StikMann::tomberEtCaFaitPouf(float xBubulle, float yBubulle, float rayonBubulle){
    y_ = y_ - (yBubulle + rayonBubulle);
    x_ = xBubulle;
}

void StikMann::sauterStyleYiha(){
    x_ = x_ + 30;
}


void StikMann::mourirCommeUneCrepe() {
    enVie = false;
    if(compteurSaut_<20){
        y_-=5.0;
        compteurSaut_++;
    }else{
        y_+=5.0f;
    }


}

void StikMann::iteration(bool saut, int xDir, Bubulle *  bulleDessous ) {

    if(saut){
        saut_=true;
        y_-=5.0f;
        compteurSaut_++;
    }
    if(compteurSaut_==30){
        saut_=false;
        compteurSaut_=0;
    }
    if(!saut_) {
        if (bulleDessous != nullptr) {
            y_ -= bulleDessous->getV_();
        }
        else {
            y_ += 5.0f;
        }
    }



    if(xDir!=0){
        x_+=xDir*v_;
        if(bulleDessous!= nullptr){
            y_+=bulleDessous->getV_();
        }
    }

}

void StikMann::setX(float x_) {
     x_ = x_;
}

void StikMann::setY(float y_) {
        y_ = y_;
}


float StikMann::getLarg() const {
    return largeur_;
}

float StikMann::getHaut() const {
    return hauteur_;
}

bool StikMann::getEnVie() {
    return enVie;
}
