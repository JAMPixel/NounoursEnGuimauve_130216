//
// Created by stiven on 13/02/16.
//

#include <GL/glew.h>
#include "openGL/InitOGL.hpp"
#include "openGL/Vbo.hpp"
#include "openGL/quad.hpp"
#include "openGL/Program.hpp"
#include "physics/JouJou.hpp"
#include "openGL/Texture.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>


int main(){
    int const width = 800;
    int const height = 600;
    InitOGL ogl(width, height, "Nounours en Guimauve");
    SDL_Event ev;
    GLuint vao;
    JouJou joujou(1, width, height, 30, 50);
    glGenVertexArrays(1,&vao);

    std::vector<glm::vec2> uvBulle= {{-1.0f, 1.0f},
                                     {-1.0f, -1.0f},
                                     {1.0f, 1.0f},
                                     {1.0f, -1.0f}
    };
    float green = 112.0f/255.0f;
    float blue = 114.0f/255.0f;
    std::vector<glm::vec3> colorBulle= {{0.0f, green,blue},
                                        {0.0f, green,blue},
                                        {0.0f, green,blue},
                                        {0.0f, green,blue}

    };
    Vbo vbo(vao,0,3,mat::rect);
    Vbo vboUv(vao,1,2,uvBulle);
    Vbo vboColorBulle(vao,2,3,colorBulle);
    Vbo vboUvPerso(vao,3,2,mat::uv);
    Program program("../src/openGL/VertexShader.glsl","../src/openGL/FragmentShader.glsl");
    program.useProgram();
    Texture texture("../resources/tousLesStickmans.png");
    Texture textureArcher("../resources/tousLesArchets.png");

    GLint uniScale = glGetUniformLocation(program.getProgram(),"scale");
    GLint uniTran  = glGetUniformLocation(program.getProgram(),"transformation");
    GLint uniRayon = glGetUniformLocation(program.getProgram(),"rayon");
    GLint uniChoix = glGetUniformLocation(program.getProgram(),"choix");
    GLint uniTexture = glGetUniformLocation(program.getProgram(),"nosTextures");
    GLint uniHG = glGetUniformLocation(program.getProgram(),"hg");
    GLint uniBD = glGetUniformLocation(program.getProgram(),"bd");

    transformStorage transformBulle;
    transformStorage transfoPerso;
    transformStorage transArchet;
    transformStorage transFleche;


    transfoPerso.scale = glm::vec2(joujou.getBubulleMan_().getLarg(),joujou.getBubulleMan_().getLarg());
    transformBulle.scale = glm::vec2(joujou.getVBubulle_()[0].getR_(),joujou.getVBubulle_()[0].getR_());
    transformBulle.tran = glm::vec2(joujou.getVBubulle_()[0].getX_(),joujou.getVBubulle_()[0].getY_());
    transArchet.scale = glm::vec2(100, 100);
    transArchet.tran = glm::vec2(25, height - 50);
    transFleche.scale = glm::vec2(40, 10);
    transFleche.tran = glm::vec2(25, height - 50);

    glUniform2f(uniBD,0.25f,0.5f);
    glUniform1f(uniRayon,joujou.getVBubulle_()[0].getR_());
    glUniformMatrix3fv(uniScale,1,GL_FALSE,glm::value_ptr(normalizedScreenSpace(height, width)));
    int x = 50;
    int y = height - 50;
    int pasX;
    int pasY;
    transformBulle.tran = glm::vec2(x,y);
    float deplaceUv = 0.25f;
    glUniform2f(uniHG,deplaceUv,0.0f);
    bool run = true;
    int time  = 0;
    bool pause = false;
    const Uint8* appuiTouche = SDL_GetKeyboardState(nullptr);
    bool change = true;
    bool deplace;
    int sensDeplace = 0;
    bool debutJeu = true;
    int eraseBubulle = 0;
    int indiceEraseBubulle =joujou.depopBubble();
    double angle ;
    pasX = -(x-(int)joujou.getVBubulle_()[indiceEraseBubulle].getX_())/60;
    pasY = (y-(int)(joujou.getVBubulle_()[indiceEraseBubulle].getY_()+60*joujou.getVBubulle_()[indiceEraseBubulle].getV_()))/60;
    angle = (std::atan(pasY/pasX)*180/M_PI);
    transFleche.angle = angle;


    float getArchet2 = 0.0f;

    while(run){

        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(1.0f,1.0f,1.0f,1.0f);

        SDL_Delay(17);
        eraseBubulle++;
       if(eraseBubulle%120 >60){
           x+=pasX;
           y+=pasY;
           std::cout <<"************************X" << x<<std::endl;
           std::cout <<"************************Y" << y<<std::endl;
           getArchet2 = 1.0f / 3.0f;
           textureArcher.bindTexture();
           glUniform1i(uniTexture, 0);
           glUniformMatrix3fv(uniTran, 1, GL_FALSE, glm::value_ptr(transform(transFleche, 0.5f, 0.5f)));
           glUniform2f(uniHG, 308.0f / 450.0f, 92.0f / 200.0f);
           glUniform2f(uniBD, 1.0f / 3.0f, (113.0f / 200.0f) - (92.0f / 200.0f));
           glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
           transFleche.tran = glm::vec2(x, y);
        }
       else
           getArchet2 = 0.0f;


        glUniform2f(uniBD,0.25f,0.5f);

        while(SDL_PollEvent(&ev)){
            switch(ev.type){
                case SDL_QUIT:
                    run = false;
                    break;
                case SDL_KEYDOWN:
                        if(ev.key.keysym.sym==SDLK_ESCAPE){
                            run = false;
                        }else if(ev.key.keysym.sym==SDLK_p){
                            pause = !pause;
                        }
            }
        }
        if(!pause && joujou.getBubulleMan_().getEnVie()) { // si pause == false et que le joueur est en vie


            transFleche.tran=glm::vec2(x,y);
            if(!(eraseBubulle%120)){
                joujou.detruire(indiceEraseBubulle);
                indiceEraseBubulle= joujou.depopBubble();
                x = 50;
                y = height-50;
                pasX = -(x-(int)joujou.getVBubulle_()[indiceEraseBubulle].getX_())/60;
                pasY = (y-(int)(joujou.getVBubulle_()[indiceEraseBubulle].getY_()+60*joujou.getVBubulle_()[indiceEraseBubulle].getV_()))/60;
                angle = (std::atan(pasY/pasX)*180/M_PI);
                transFleche.angle = angle;
            }
            deplace = false;
            joujou.iteration();
            joujou.getBubulleMan_().iteration(joujou.getBubulleMan_().isSaut(),sensDeplace, joujou.getBubulleDessous());
            sensDeplace = 0;
            if(appuiTouche[SDL_SCANCODE_RIGHT]) {
                deplace =true;
                sensDeplace = 1;
                if(transfoPerso.scale.x<0 )
                    transfoPerso.scale.x*=(-1);
            }
            if(appuiTouche[SDL_SCANCODE_LEFT]) {
                sensDeplace = -1;
                deplace = true;
                if(transfoPerso.scale.x>0)
                    transfoPerso.scale.x*=(-1);
            }
            if(appuiTouche[SDL_SCANCODE_UP]){
                if(change){
                    joujou.getBubulleMan_().iteration(true, sensDeplace, joujou.getBubulleDessous());
                    change = false;
                    glUniform2f(uniHG,0.0f,0.5f);//le perso saute
                }

            }else if(!debutJeu &&( joujou.getBubulleDessous()!=nullptr || time)){ // si le perso va atterir

                if(time%30) {
                    glUniform2f(uniHG, 0.5f, 0.5f);
                    time++;
                }else{
                    time = 0;
                    change = true;
                    //joujou.getBubulleMan_().getUnderBubulle_()->reception();
                }
            }
            if(joujou.getBubulleMan_().getCompteurSaut_()!=0){ // si le perso est en saut
                if(!time)
                    time++;
                glUniform2f(uniHG,0.25f,0.5f);
            }else if(joujou.getBubulleDessous()==nullptr) {// si le perso tombe
                change = false;
                if(!time)
                    time++;
                glUniform2f(uniHG, 0.75f, 0.0f);
            }else if(!time &&  joujou.getBubulleDessous()!=nullptr && joujou.getBubulleMan_().getCompteurSaut_()==0){//si le perso
                //est immobile
                glUniform2f(uniHG, deplaceUv, 0.0f);
                if(deplace){
                    glUniform2f(uniHG,0.0f,0.0f);
                }
            }
        }else if(!joujou.getBubulleMan_().getEnVie()){/// si le perso est mort
            joujou.iteration();
            glUniform2f(uniHG,0.5f,0.0f);
        }

        glUniform1i(uniChoix,1);
        for(unsigned int i =0;i<joujou.getVBubulle_().size();i++){ // affichage bulle
            transformBulle.tran = glm::vec2(joujou.getVBubulle_()[i].getX_(),joujou.getVBubulle_()[i].getY_());
            glUniformMatrix3fv(uniTran,1,GL_FALSE,glm::value_ptr(transform(transformBulle,0.5f,0.5f)));
            glDrawArrays(GL_TRIANGLE_STRIP,0,4);

        }
        if(StikMann::getUnderBubulle_()!=nullptr) { // affichage bulle sous perso
            transformBulle.tran = glm::vec2(StikMann::getUnderBubulle_()->getX_(),
                                            StikMann::getUnderBubulle_()->getY_());
            glUniformMatrix3fv(uniTran, 1, GL_FALSE, glm::value_ptr(transform(transformBulle, 0.5f, 0.5f)));
            glUniform1i(uniChoix, 2);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
            std::cout <<StikMann::getUnderBubulle_()<<std::endl;
        }
        glUniform1i(uniChoix,0);
        transfoPerso.tran = glm::vec2(joujou.getBubulleMan_().getX(),joujou.getBubulleMan_().getY());
        glUniformMatrix3fv(uniTran,1,GL_FALSE,glm::value_ptr(transform(transfoPerso,0.5f,0.5f)));
        texture.bindTexture();
        glUniform1i(uniTexture,0); // affichage Perso
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
        glUniformMatrix3fv(uniTran,1,GL_FALSE,glm::value_ptr(transform(transArchet,0.5f,0.5f)));
        textureArcher.bindTexture();
        glUniform1i(uniTexture,0); // affichage archet
        glUniform2f(uniBD, 1.0f/3.0f, 1.0f);
        glUniform2f(uniHG, getArchet2, 0.0f);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
        debutJeu = false;
        SDL_GL_SwapWindow(ogl.getWindow());
    }
    glDeleteVertexArrays(1,&vao);

    return 0;
}